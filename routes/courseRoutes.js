const express = require("express");
const router = express.Router();
const CourseControllers = require("../controllers/CourseControllers");
const auth= require("../auth");

// Adding course
router.post("/", auth.verify,CourseControllers.addCourse);
router.get("/allActiveCourses" ,CourseControllers.getAllActive);



// retrieve all courses
router.get("/allCourses", auth.verify, CourseControllers.getAllCourses)

// update specific course
router.put("/update/:courseId", auth.verify, CourseControllers.updateCourse)



router.get("/:courseId" ,CourseControllers.getCourse);

router.patch("/:courseId/patch", auth.verify, CourseControllers.archiveCourse)


module.exports = router;
