const express = require('express')
const router= express.Router();
const userController = require("../controllers/UserControllers")
const auth= require("../auth");

// const {checkEmailExists,registerUser,userController}= userController;
// const cors = require('cors');


// router.get('/', );
router.post("/checkemail", userController.checkEmailExists);
router.post("/register",
userController.checkEmailExists,
userController.registerUser);
router.post("/login", userController.loginUser);
router.post("/getdetails", userController.getDetails);
router.get("/profile",auth.verify,userController.profileDetails);

router.patch("/updateRole/:userId",auth.verify,userController.updateRole);

router.post("/enroll/:courseId",auth.verify, userController.enroll);
module.exports = router;