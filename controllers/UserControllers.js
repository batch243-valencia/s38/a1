const User = require('../models/Users')
const Course = require('../models/Courses')
const bcrypt = require("bcrypt");
// const { response } = require('express');
const auth = require("../auth");
const { findByIdAndUpdate, updateOne } = require('../models/Users');
const { response } = require('express');

// check email if already exist
const checkEmailExists = async (req,res, next) => {
    
    console.log([this.function].name)
    const result = await User.find({ email: req.body.email });
    console.log(req.body.email);
    let message_1 = "";
    if (result.length > 0) {
        message_1 = `The ${req.body.email} is already been taken, please use other email.`;
        return res.send(message_1);
    } else {
        next();
    }
}

const registerUser = (req,res)=> {
    
    console.log([this.function].name)
    let newUser = new User({
        firstName: req.body.firstName,
        lastName: req.body.lastName,
        email: req.body.email,
        password: bcrypt.hashSync(req.body.password, 10),
        mobileNo: req.body.mobileNo
       
    })
    return newUser.save().then(user=>{
        console.log(user)
         res.send(`Congratulation ${newUser.firstName}`)
    }).catch(err=>{
        console.log(err);
        res.send(`Sorry ${newUser.firstName} there was an  error during the registration`)
    })
}

const loginUser = (request, response) =>{
  
    console.log([this.function].name)
	// The findOne method, returns the first record in the collection that matches the search criteria.

	return User.findOne({email : request.body.email})
	.then(result =>{

		console.log(result);

		if(!result){
			response.send(`Your email: ${request.body.email}, is not yet registered. Register first!`);
		}
		else{
			const isPasswordCorrect = bcrypt.compareSync(request.body.password, result.password);

			if(isPasswordCorrect){
				let token = auth.createAccessToken(result);
				console.log(token);
				return response.send({accessToken: token});
			}
			else{
				return response.send(`Incorrect password, please try again!`);
			}
		}
	})
}


const getDetails =(req,res)=>{

    console.log([this.function].name)
 return User.findOne({ "_id": req.body._id })
    .then(result =>{
        {result.password=""}
        return res.status(200).send(result)
            }).catch(err=>
        {
            return res.status(404).json('User Doesnt exist')
        }
    )
}

const profileDetails = (request, response) =>{
   
    console.log([this.function].name)
	// user will be object that contains the id and email of the user that is currently logged in.
	const userData = auth.decode(request.headers.authorization);
	console.log(userData);
	return User.findById(userData.id).then(result => {
		result.password = "Confidential";
		return response.send(result)
	}).catch(err => {
		return response.send(err);
	})
	
}


const updateRole = (req, res) =>{
   
    console.log([this.function].name) 
    let token = req.headers.authorization;
    let userData = auth.decode(token);

    let idToBeUpdated = req.params.userId;

    if(userData.isAdmin){
        return User.findById(idToBeUpdated).then(result =>{
            let update = {
                isAdmin: !result.isAdmin
            };
            console.log(`update is: ${update}`)
            return User.findByIdAndUpdate(idToBeUpdated, update, {new: true})
            .then(document => {
                document.password = "Confidential";
                res.send(document)
            }).catch(err => {
                res.send(err);
                console.log(err);
            });
        }).catch(err => {
            res.send(err);
            console.log(err);
        });

    }else{
        return res.send("User does not have access");
    }
}

const enroll = async(req,res)=>{
    const token = req.headers.authorization;
    const userData=auth.decode(token);
    const courseid =req.params.courseId

    try {        
        const course =  await Course.findById(courseId);

        if(!course)return res.send("Course Not Found")
        course.enrollees.push({userId: userData._id});
        course.slots -=1;
        await course.save();
        
        const user = await User.findById(userData._id);

        if(!user) return res.send("alskd");
        user.enrollments.push({courseId: course.id});
        await user.save();
        res.send("Enrolled")

    }catch(error){
        res.json({message: error.message})
    }
}
module.exports = {checkEmailExists, registerUser, loginUser, getDetails,profileDetails,updateRole,enroll}