// Setup dependencies
require('dotenv').config()
const express = require('express');
const mongoose = require('mongoose');
// const cors = require("cors");


// Allows all resources to access our backend application
const app=express()
const userRoutes =  require("./routes/userRoutes")
const courseRoutes =  require("./routes/courseRoutes")

//check/Initialize Connection
mongoose.connection.on("error", console.error.bind(console, "connection error"));
mongoose.connection.once('open',()=>console.log("Now Connected to MongoDb Atlas"));

// app.use(cors());
app.use(express());
app.use(express.json());
app.use(express.urlencoded({extended:true}));

app.use("/users", userRoutes)

app.use("/courses", courseRoutes)


// connect to db
mongoose.connect(process.env.DATABASE_URL,{
        useNewUrlParser:true,
        useUnifiedTopology:true
    })
  .then(() => {
    console.log('connected to database')
    // listen to port
    const port = process.env.PORT
    app.listen(process.env.PORT, () => {
      console.log(`listening for requests on port localhost:${port}`)
    })
  })
  .catch((err) => {
    console.log(err)
  }) 